/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


   
   var ul=document.getElementById('ul');
   var buttn=document.getElementById('button');
   var scoreCard=document.getElementById('scoreCard');
   var quizBox=document.getElementById('questionBox');
  var op1=document.getElementById('op1');
  var op2=document.getElementById('op2');
  var op3=document.getElementById('op3');
  var op4=document.getElementById('op4');


      var app={                            // to add questions along with options and correct answer with it
                questions:[
                    {q:'Full Form of HTML', options:['Header Text Markup Language','Higher Text Markup Language','Hyper Text Markup Language','None of these'],answer:3},
					
					{q:' Which of the following is a container? ',options:['Select', 'Body', 'input',' both Select and Body'],answer:4},

                    {q:'which of the following tag is used to write a pragraph in html ?',options:['<body>','<br>','<em>','<p>'],answer:4},
						  
					{q:' Which of these HTML tag is used to add unordered list ?',options:['ol','li','ul','script'],answer:3},

                    {q:' Which of these HTML tag provide the largest heading ?',options:['h1','h8','h2','h9'],answer:1}
                         ],
                index:0,
                load:function(){
                	   if(this.index<=this.questions.length-1){
                        quizBox.innerHTML=this.index+1+". "+this.questions[this.index].q;      
                        op1.innerHTML=this.questions[this.index].options[0];
                        op2.innerHTML=this.questions[this.index].options[1];
                        op3.innerHTML=this.questions[this.index].options[2];
                        op4.innerHTML=this.questions[this.index].options[3];
                           this.scoreCard();
                        }
                        else{

                        quizBox.innerHTML="Quiz End"  ;      //after all questions are done it will show quiz end
                        op1.style.display="none"; 
                        op2.style.display="none";
                        op3.style.display="none";
                        op4.style.display="none";
                        buttn.style.display="none";
                        }
                },
                 next:function(){
                    this.index++;
                    this.load();
                 },
                check:function(ele){
                   
                         var id=ele.id.split('');
                   //    to check and count the score if option selected is correct   
                         if(id[id.length-1]===this.questions[this.index].answer){               
                         	this.score++;
                         	ele.className="correct";
                         	ele.innerHTML="Correct";
                         	this.scoreCard();
                         }  
						 // if the option selected is incorrect
                         else{
                         	ele.className="wrong";
                         	ele.innerHTML="Wrong";
                         }
                },
				  //clickable is used to allow user to get the chance to select answer,
				 // otherwise without these the answer will appear automatically when quiz is started
             notClickAble:function(){
                     for(let i=0;i<ul.children.length;i++){
                      ul.children[i].style.pointerEvents="none";
                       }
                },

              clickAble:function(){
                    for(let i=0;i<ul.children.length;i++){
                      ul.children[i].style.pointerEvents="auto";
                      ul.children[i].className='';//First commit(Adding semicolons at end of statement)

                       }
                },
             score:0,                    //to show the score 
               scoreCard:function(){
               scoreCard.innerHTML=this.score+" out of "+this.questions.length;  
                }
 
           };


           window.onload=app.load();

           function button(ele){
           	     app.check(ele);
           	     app.notClickAble();
           }

         function  next(){
              app.next();
              app.clickAble();
         } 



